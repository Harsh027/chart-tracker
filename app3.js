let table = document.querySelector("#sdata");
let otable = document.querySelector("#outer");

function renderData(doc) {
  const sb = firebase.firestore();
  sb.collection(doc.id)
    .doc("Setup")
    .get()
    .then((snapshot1) => {
      const bs = snapshot1.data().BS === "Buy" ? "buy" : "sell";
      let code =
        '<div class="row" id="' +
        doc.id +
        '_row"> \
          <div id="firstc" class="column" > \
            <span id="n1">' +
        doc.id +
        '</span><span id="n2"> \
            <iframe id= "i1" src="' +
        snapshot1.data().Youtube +
        '"  style="border:none;background: rgb(98,76,171);background: linear-gradient(20deg, rgba(98,76,171,1) 25%, rgba(156,144,204,1) 64%, rgba(163,166,201,1) 100%);" allowfullscreen> \
            </iframe></span>\
          </div>\
          <div id="secondc" class="column" >\
          <div class="method">\
            <span class="option_name">Indicator Used</span>' +
        "<span class='option_desc'>" +
        snapshot1.data().Indicator +
        "</span>" +
        "</div>\
          <div class='method'>\
            <span class='option_name'>Time</span>" +
        "<span class='option_desc'>" +
        snapshot1.data().Time +
        "</span>" +
        "</div>\
          <div class='method'>\
            <span class='option_name'>Candlestic</span>" +
        "<span class='option_desc'>" +
        snapshot1.data().ChartType +
        "</span>" +
        "</div>\
          <div class='method'>\
            <span class='option_name'>Buy/Sell</span>" +
        "<span class='option_desc " +
        bs +
        "'>" +
        snapshot1.data().BS +
        "</span>" +
        "</div>\
          <div class='method'>\
            <span class='option_name'>Buy</span>" +
        "<span class='option_desc'>" +
        snapshot1.data().Buy +
        "</span>" +
        "</div>\
          <div class='method'>\
            <span class='option_name'>Sell</span>" +
        "<span class='option_desc'>" +
        snapshot1.data().Sell +
        "</span>" +
        "</div>\
          </div>\
          <div id='thirdc' class='column'>\
          <a>Profit So far</a>\
          <table id='" +
        doc.id +
        "'> \
                          <thead><tr class='heading'> \
                              <th>Stock</th> <th>Total Profits</th><th>Accuracy</th> <th> More Details </th>\
                            </tr></thead>\
                            </table>";

      const tb = firebase.firestore();
      tb.collection(doc.id)
        .get()
        .then((snapshot) => {
          let table1 = "";
          snapshot.docs.forEach((element) => {
            if (!element.id.includes("Setup")) {
              console.log(element.id);
              table1 += "<tr class='striped'>";
              table1 += "<td>" + element.id + "</td>";
              table1 += "<td>" + element.data().Profits + "</td>";
              table1 += "<td>" + element.data().Accuracy + "</td>";
              table1 += "<td><a href='page2.html'>Click Me!</a></td>";
              table1 += "</tr>";
            }
          });
          document
            .getElementById(doc.id)
            .insertAdjacentHTML("beforeend", table1);
        });
      code += "  </div>\
                    </div>";

      document.getElementById("d_row").insertAdjacentHTML("beforeend", code);
    });
}

db.collection("Allc")
  .get()
  .then((snapshot) => {
    snapshot.docs.forEach((element) => {
      console.log(element.id);
      renderData(element);
    });
  });
