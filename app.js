var s = "Open=Previous Close(OC)";
var symbol = "Nifty";
var year = "2020";
let table = document.querySelector("#Data");
var month = "Mar".toString();
function renderData(doc) {
  let li = document.createElement("tr");
  let day = document.createElement("td");
  let buyt = document.createElement("td");
  let buy = document.createElement("td");
  let sell = document.createElement("td");
  let sellt = document.createElement("td");
  let qty = document.createElement("td");
  let profits = document.createElement("td");
  let stock = document.createElement("td");

  var ndate = doc.data().Date;
  ndate = ndate.slice(0, -1);
  day.textContent = ndate;
  buyt.textContent = doc.data().BuyT;
  buy.textContent = doc.data().Buyp;
  sell.textContent = doc.data().Sell;
  sellt.textContent = doc.data().SellT;
  profits.textContent = doc.data().profit;
  stock.textContent = doc.data().Stock;
  qty.textContent = doc.data().qty;
  // alert(doc.data().profit);
  //      alert(stock.textContent.includes(month));
  if (day.textContent.includes(month)) {
    li.appendChild(day);
    li.appendChild(stock);
    li.appendChild(buyt);
    li.appendChild(buy);
    li.appendChild(sell);
    li.appendChild(sellt);
    li.appendChild(qty);
    li.appendChild(profits);
    table.appendChild(li);
  }
}
db.collection(s)
  .doc(symbol)
  .collection(year)
  .get()
  .then((snapshot) => {
    snapshot.docs.forEach((element) => {
      console.log(element.data());
      renderData(element);
    });
  });
